#	A Website for Digital Editions

Built using the Ionic framework:
https://ionicframework.com/

For a quickstart:

1.	Check out the template project here: [https://bitbucket.org/slssu/gde_project_template/src/master/](https://bitbucket.org/slssu/gde_project_template/src/master/)

To run the website on your own computer:

1.	Download Node.js
	*	https://nodejs.org/en/download/
2.	Clone the repository
3.	Run the following in the folder: npm install -g cordova ionic
5.	Copy the src/config-sample.json to the newly created www/ folder, rename it as config.json
4.	Run "ionic serve" to start the application in a web browser

**Note!**
The website requires a connection to the API that is freely available here:

https://bitbucket.org/slssu/sls_api

By default the website will connect to the SLS API for the Topelius mobile web.

## Screenshot(s) of the Web front-end
![Generic Digital Edition - web.png](https://bitbucket.org/repo/5qeMpzy/images/3348979668-Generic%20Digital%20Edition%20-%20web.png)
