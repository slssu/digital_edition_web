# How to use this file...
#
# "make" (or "make sync"):
# Copies any updated files on your server to your local machine, and then
# uploads any local changes to the server.
#
# "make drysync":
# Shows (but doesn't transfer) the files that will go back and forth if
# you were to sync for real.
#
# "make get":
# Just downloads any updated files from your server. ("make dryget" will
# do a dry-run of this.)
#
# "make put":
# Just uploads any updated files from your local machine. ("make dryput"
# will do a dry-run of this.)


# User-set makefile variables...
#
# localdir:    path to site files on personal machine (end with slash)
# remoteuser:  username for logging into server
# remotehost:  server host name
# remotedir:   path to site files on server (end with slash)
#
# switches:    rsync switches for both uploading and downloading files
# getswitches: rsync switches for downloading files only
# putswitches: rsync switches for uploading files only
#
# getmessage:  message to echo before downloading files
# putmessage:  message to echo before uploading files
#
# Optional comand line - tdir - specific target directory
tdir    :=/

localdir     = www
remoteuser   = genero
remotehost   = 10.0.32.101
remotedir    = /var/www/digital-editions/ui/www

switches     = --checksum -rlvzu --exclude ".*" --exclude "*.tgz*" --exclude "Makefile.*"
getswitches  = --exclude "logs"
putswitches  = --exclude "logs"

getmessage   = Updating local site mirror with newer files from server...
putmessage   = Updating server with newer files from local site mirror...

# In normal circumstances, nothing below this comment should need modification:
# essentially everything is configurable using the variables above.

deploy:    put
drydeploy:   dryput

# "--update" is hardcoded into the downloading commands because if it weren't
# there, local changes would be overwritten by (older) live versions of files
# before ever getting uploaded to the server.

sd = $(patsubst %/,%,$(tdir))


put:
	@echo "$(putmessage)"
	rsync $(putswitches) $(switches) $(localdir)$(sd)/ $(remoteuser)@$(remotehost):$(remotedir)$(sd)/
	@echo

dryput:
	@echo "DRY RUN: $(putmessage)"
	rsync --dry-run $(putswitches) $(switches) $(localdir)$(sd)/ $(remoteuser)@$(remotehost):$(remotedir)$(sd)/
	@echo

.PHONY: sync drysync get dryget put dryput tdir
