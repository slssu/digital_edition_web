import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TableOfContentsService } from './table-of-contents.service';
import { TableOfContentsList } from './table-of-contents.component';
import { TableOfContentsItem, TableOfContentsSubcategory, TableOfContentsCategory, GeneralTocItem } from './table-of-contents.model';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Http} from '@angular/http';

export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    schemas: [
        NO_ERRORS_SCHEMA
    ],

    declarations: [
      TableOfContentsList
    ],
    imports: [
        IonicModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [Http]
            }
        })
    ],
    entryComponents: [
        TableOfContentsList
    ],
    providers: [
      TableOfContentsService
    ],
    exports: [
        TableOfContentsList
    ],
  })
  export class TableOfContentsModule {}
