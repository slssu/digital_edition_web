import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '@ngx-config/core';
import { TableOfContentsCategory, GeneralTocItem } from './table-of-contents.model';


@Injectable()
export class TableOfContentsService {

  private tableOfContentsUrl = '/table-of-contents/edition/';  // plus an id...
  private prevNextUrl = '/table-of-contents/';  // plus an id...

  constructor(private http: Http, private config: ConfigService) {
  }

  getTableOfContents (id: string): Observable<TableOfContentsCategory[]> {
    return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
                           this.config.getSettings('app.machineName') +
                           this.tableOfContentsUrl + id)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getTableOfContentsRoot (id: string): Observable<GeneralTocItem[]> {
    return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
                           this.config.getSettings('app.machineName') +
                           this.tableOfContentsUrl + id + '/root')
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getTableOfContentsGroup (id: string, group_id: string): Observable<GeneralTocItem[]> {
    return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
                           this.config.getSettings('app.machineName') +
                           this.tableOfContentsUrl + id + '/group/' + group_id)
                    .map(this.extractData)
                    .catch(this.handleError);
  }


  getPrevNext (id: string): Observable<TableOfContentsCategory[]> {

    const arr = id.split('_');
    const ed_id = arr[0];
    const item_id = arr[1];

    return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
                           this.config.getSettings('app.machineName') +
                           this.tableOfContentsUrl + ed_id + '/prevnext/' + item_id)
                    .map(this.extractData)
                    .catch(this.handleError);
  }


  getFirst (editionId: string): Observable<any[]> {

        return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
                               this.config.getSettings('app.machineName') +
                               this.tableOfContentsUrl + editionId + '/first')
                        .map(this.extractData)
                        .catch(this.handleError);
  }


  private extractData(res: Response) {
    const body = res.json();
    return body;
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
