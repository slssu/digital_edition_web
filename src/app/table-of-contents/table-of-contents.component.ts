import { Component, Input } from '@angular/core';

import { App } from 'ionic-angular';

import { DigitalEdition } from '../digital-edition/digital-edition.model';
import { TableOfContentsCategory, GeneralTocItem } from './table-of-contents.model';
import { TableOfContentsService } from './table-of-contents.service';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';

import { SingleEditionPagePart } from '../../pages/single-edition-part/single-edition-part';
import { ReadPage } from '../../pages/read/read';
import { LanguageService } from '../../app/language/language.service';


@Component({
  selector: 'table-of-contents',
  templateUrl: `table-of-contents.component.html`
})
export class TableOfContentsList {
  errorMessage: string;
  @Input() edition: DigitalEdition;
  @Input() tableOfContents: TableOfContentsCategory[];
  @Input() root: TableOfContentsCategory[];

  @Input() tocItems: GeneralTocItem[];
  @Input() showIntroduction: boolean;

  language: string;

  constructor(private app: App, private tableOfContentsService: TableOfContentsService, public translate: TranslateService,
    public languageService: LanguageService) {
    this.languageService.getLanguage().subscribe((lang: string) => {
      this.language = lang;
    });
  }

  ionViewWillEnter() {
  }

  openIntroduction() {
    const params = {root: this.root, tocItem: null, edition: {title: 'Introduction'}};
    params['editionId'] = this.edition.id;
    params['id'] = 'introduction';
    params['firstItem'] = '1'; // this.getFirstLink(this.tocItems);
    const nav = this.app.getActiveNavs();
    nav[0].push('read', params, {animate: true, direction: 'forward', animation: 'ios-transition'});
  }

  getFirstLink(items) {
    const len = items.length
    let i, j, len2;

    for (i = 0; i < len; i++) {
      if (items[i].toc_linkID) {
        return items[i];
      }
      const children = items[i].items;
      console.log(children);
      for (j = 0, len2 = children.length; j < len2; j++) {
        if (children[i].toc_linkID) {
          return children[i];
        }
      }
    }
  }

  openTocItem(tocItem: any) {
    const params = {root: this.root, tocItem: tocItem, edition: {title: tocItem.title}};
    const nav = this.app.getActiveNavs();

    if (tocItem.toc_linkID) {
      params['tocLinkId'] = tocItem.toc_ed_id + '_' + tocItem.toc_linkID;
      params['id'] = tocItem.toc_linkID;
      params['editionId'] = tocItem.toc_ed_id;
      nav[0].push('read', params, {animate: true, direction: 'forward', animation: 'ios-transition'});
    } else {
      params['editionId'] = this.edition.id;
      params['id'] = tocItem.toc_id;

      nav[0].push('single-edition-part', params, {animate: true, direction: 'forward', animation: 'ios-transition'});
    }
  }
}
