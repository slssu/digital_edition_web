
export class TableOfContentsItem {
  title: string;
  id: string;
  titleLevel: string;

  constructor(tocInfo: any) {
    this.title = tocInfo.title;
    this.id = tocInfo.id;
    this.titleLevel = tocInfo.titleLevel;
  }
}
