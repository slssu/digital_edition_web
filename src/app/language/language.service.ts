import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '@ngx-config/core';
import { LangChangeEvent, TranslateService/*, TranslatePipe*/ } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';


@Injectable()
export class LanguageService {

  private langCode: string;
  private langChangeEnabled: false;

  constructor(public translate: TranslateService, public storage: Storage, private config: ConfigService) {
    translate.addLangs(this.config.getSettings('i18n.languages'));
    translate.setDefaultLang(this.config.getSettings('i18n.locale'));

    this.langChangeEnabled = this.config.getSettings('i18n.enableLanguageChanges');

    this.getLanguage().subscribe((lang: string) => {
      this.setLanguage(lang);
      this.storage.set('language', translate.currentLang);
      this.langCode = translate.currentLang;
    });

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.storage.set('language', translate.currentLang);
      this.langCode = translate.currentLang;
    });
  }

  public getLanguage(): Observable<string> {
    const translate = this.translate;
    const storage = this.storage;
    const langCode = this.langCode;
    return Observable.create(function (subscriber) {
      if (!this.langChangeEnabled) {
        subscriber.next(translate.getDefaultLang());
        subscriber.complete();
        return;
      }

      if (langCode) {
        subscriber.next(langCode);
        subscriber.complete();
        return;
      }

      storage.get('language').then((lang) => {
        if (lang) {
          subscriber.next(lang);
          subscriber.complete();
        } else {
          const browserLang = translate.getBrowserLang();
          subscriber.next(browserLang.match(/sv|fi/) ? browserLang : translate.getDefaultLang())
          subscriber.complete();
        }
      }).catch(() => {
        subscriber.next('sv');
        subscriber.complete();
      });
    });
  }

  public setLanguage(lang: string) {
    this.translate.use(lang);
  }

  public get(text: string) {
    return this.translate.get(text);
  }
}
