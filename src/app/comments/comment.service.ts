import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '@ngx-config/core';
import { Comment } from './comment.model';
import { CommentCacheService } from './comment-cache.service';

@Injectable()
export class CommentService {

  private readTextUrl = '/text/com/';
  textCache: any;

  constructor(private http: Http, private config: ConfigService, private cache: CommentCacheService) {

  }


  getComment(id: string): Observable<any> {

    const id2 = id.replace('_com', '');
    const parts = id2.split(';');

    const commentId = parts[0];

    if (this.cache.hasHtml(commentId)) {
      // console.lof
      return this.cache.getHtmlAsObservable(id2);
    } else {
      return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
          this.config.getSettings('app.machineName') + this.readTextUrl + parts[0] + '/' + parts[1])
          .map(res => {
      const selector: string = '.' + parts[1];
            const body = res.json();
      const range = document.createRange();
      const docFrags = range.createContextualFragment(body.content);
      if ( docFrags.querySelector(selector) ) {
        const strippedBody = docFrags.querySelector(selector).innerHTML;
        if ( strippedBody !== undefined) {
          return strippedBody || ' - no content - ';
        }
      }
            return body.content || ' - no content - ';
          })
          .catch(this.handleError);
    }
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
