import { Component, OnInit } from '@angular/core';

import { App } from 'ionic-angular';
import { TranslateService} from '@ngx-translate/core';
import { ConfigService } from '@ngx-config/core';
import { DigitalEdition } from '../digital-edition/digital-edition.model';
import { DigitalEditionListService } from './digital-edition-list.service';

// import { SingleEditionPage } from '../../pages/single-edition/single-edition';


@Component({
  selector: 'digital-editions-list',
  templateUrl: `./digital-edition-list.html`
})
export class DigitalEditionList implements OnInit {
  errorMessage: string;
  digitalEditions: DigitalEdition[];
  projectMachineName: string;
  editionImages: any;
  editionShortTexts: any;
  appLanguage: any;

  constructor(
    private app: App,
    private digitalEditionListService: DigitalEditionListService,
    private config: ConfigService,
    public translate: TranslateService
  ) {
    this.projectMachineName = this.config.getSettings('app.machineName');
    this.editionImages = this.config.getSettings('editionImages');
    this.editionShortTexts = this.config.getSettings('editionShortTexts');
    this.appLanguage = this.config.getSettings('i18n').locale;
  }

  ngOnInit() {
    this.getDigitalEditions();
  }

  getDigitalEditions() {
    this.digitalEditionListService.getDigitalEditions()
        .subscribe(
          digitalEditions => {this.digitalEditions = digitalEditions; },
          error =>  {this.errorMessage = <any>error});
  }

  openEdition(edition: DigitalEdition) {
    const nav = this.app.getActiveNavs();
    const params = {edition: edition, fetch: true, id: edition.id};
    nav[0].push('single-edition', params, {animate: true, direction: 'forward', animation: 'ios-transition'});

  }
}
