
export class DigitalEdition {
  title: string;
  id: string;
  image: string;
  divchapters: boolean;

  constructor(pubInfo: any) {
    this.title = pubInfo.title;
    this.id = pubInfo.id;
    this.divchapters = (pubInfo.divchapters === '1');
  }
}
