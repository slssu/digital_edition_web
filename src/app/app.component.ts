import { Component, ViewChild } from '@angular/core';
import './rxjs-operators';
import { Nav, Platform } from 'ionic-angular';
import { LangChangeEvent, TranslateService/*, TranslatePipe*/ } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../pages/tabs/tabs';
import { LanguageService } from './language/language.service';
import { Title } from '@angular/platform-browser';


@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class DigitalEditionsApp {
  @ViewChild(Nav) nav: Nav;

  rootPage = 'TabsPage';

  constructor(public platform: Platform, public translate: TranslateService, public storage: Storage,
    public languageService: LanguageService, private titleService: Title) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // StatusBar.styleDefault();
      // SplashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
