import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '@ngx-config/core';
import { EstablishedText } from './established-text.model';
import { TextCacheService } from './text-cache.service';

@Injectable()
export class TextService {

  private readTextUrl = '/text/est/';
  private introductionUrl = '/text/inl/';

  textCache: any;

  constructor(private http: Http, private config: ConfigService, private cache: TextCacheService) {

  }


  getEstablishedText(id: string): Observable<any> {

    const id2 = id.replace('_est', '');
    const parts = id2.split(';');

    const textId = parts[0];

    if (this.cache.hasHtml(textId)) {
      return this.cache.getHtmlAsObservable(id);
    } else {
      return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
          this.config.getSettings('app.machineName') + this.readTextUrl + textId + '_est')
          .map(res => {
            const body = res.json();
            this.cache.setHtmlCache(textId, body.content);
            return this.cache.getHtml(id);

            // return body.content || " - no content - ";
          })
          .catch(this.handleError);
    }
  }

  getIntroduction(id: string, lang: string): Observable<any> {
      return this.http.get(  this.config.getSettings('app.apiEndpoint') + '/' +
          this.config.getSettings('app.machineName') + this.introductionUrl + id + '/' + lang)
          .map(res => {
            return res.json();
          })
          .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
