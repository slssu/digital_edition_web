import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TableOfContentsModule } from './table-of-contents/table-of-contents.module';

import { HttpModule, Http } from '@angular/http';
import { BrowserModule, Title } from '@angular/platform-browser';

import { DigitalEditionsApp } from './app.component';

import { TextService } from './texts/text.service';
import { TextCacheService } from './texts/text-cache.service';
import { HtmlContentService } from './html-content/html-content.service';
import { LanguageService } from './language/language.service';
import { ReadPopoverService, Fontsize } from '../pages/read-popover/read-popover.service';
import { CommentService } from './comments/comment.service';
import { SemanticDataService } from './semantic-data/semantic-data.service';
import { CommentCacheService } from './comments/comment-cache.service';
import { TooltipService } from './tooltip/tooltip.service';


import { ReadPopoverPage } from '../pages/read-popover/read-popover';
import { CommentModalPage } from '../pages/comment-modal/comment-modal';
import { SemanticDataModalPage } from '../pages/semantic-data-modal/semantic-data-modal';

import { ConfigLoader, ConfigModule  } from '@ngx-config/core';
import { ConfigHttpLoader } from '@ngx-config/http-loader';


export function createTranslateLoader(http: Http): TranslateLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function createConfigLoader(http: Http): ConfigLoader {
    return new ConfigHttpLoader(http, 'config.json');
}


@NgModule({
  declarations: [
    DigitalEditionsApp,
    ReadPopoverPage,
    CommentModalPage,
    SemanticDataModalPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    }),
    TableOfContentsModule,
    ConfigModule.forRoot({
        provide: ConfigLoader,
        useFactory: (createConfigLoader),
        deps: [Http]
    }),
    IonicModule.forRoot(
      DigitalEditionsApp, {
      backButtonText: 'Tillbaka',
      tabsPlacement: 'bottom',
      mode: 'md'
    })
  ],
  providers: [
    HtmlContentService,
    TextService,
    TextCacheService,
    TranslateService,
    LanguageService,
    ReadPopoverService,
    Title,
    CommentService,
    CommentCacheService,
    SemanticDataService,
    TooltipService
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    DigitalEditionsApp,
    ReadPopoverPage,
    CommentModalPage,
    SemanticDataModalPage

  ]
})
export class AppModule {}
