import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '@ngx-config/core';
import { SemanticData } from './semantic-data.model';

@Injectable()
export class SemanticDataService {

  private semanticDataUrl = '/semantic_data/';
  textCache: any;

  constructor(private http: Http, private config: ConfigService) {

  }


  getPlace(id: string): Observable<any> {
    return this.http.get(  this.config.getSettings('app.apiEndpoint') +  // We do no use project name for semantic data
        this.semanticDataUrl + 'places/tooltip/' + id)
        .map(res => {
          const body = res.json();

          return body.content || ' - no content - ';
        })
        .catch(this.handleError);
  }

  getPerson(id: string): Observable<any> {
    return this.http.get(  this.config.getSettings('app.apiEndpoint') +  // We do no use project name for semantic data
        this.semanticDataUrl + 'persons/tooltip/' + id)
        .map(res => {
          const body = res.json();

          return body || ' - no content - ';
        })
        .catch(this.handleError);
  }

  getSemanticData(id: string): Observable<any> {
    return this.http.get(  this.config.getSettings('app.apiEndpoint') +  // We do no use project name for semantic data
        this.semanticDataUrl + 'persons/tooltip/' + id)
        .map(res => {
          const body = res.json();

          return body.content || ' - no content - ';
        })
        .catch(this.handleError);

  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
