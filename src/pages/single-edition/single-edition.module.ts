import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleEditionPage } from './single-edition';
import { TableOfContentsModule } from '../../app/table-of-contents/table-of-contents.module';
import { TableOfContentsService } from '../../app/table-of-contents/table-of-contents.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http } from '@angular/http';

export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    declarations: [
        SingleEditionPage
    ],
    imports: [
      IonicPageModule.forChild(SingleEditionPage),
      TableOfContentsModule,
      TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [Http]
        }
      })
    ],
    providers: [
        TableOfContentsService
    ],
    entryComponents: [
        SingleEditionPage
    ]
  })
  export class SingleEditionPageModule {}
