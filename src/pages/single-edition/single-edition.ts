import { Component } from '@angular/core';
import { App, NavController, ViewController, NavParams, PopoverController, IonicPage } from 'ionic-angular';

import { DigitalEdition } from '../../app/digital-edition/digital-edition.model';
import { TableOfContentsCategory, GeneralTocItem } from '../../app/table-of-contents/table-of-contents.model';
import { TableOfContentsService } from '../../app/table-of-contents/table-of-contents.service';
import { TableOfContentsList } from '../../app/table-of-contents/table-of-contents';
import { TextService } from '../../app/texts/text.service';
import { HtmlContentService } from '../../app/html-content/html-content.service';
import { ConfigService } from '@ngx-config/core';
import { global } from '../../app/global';
import { Events } from 'ionic-angular';
import { TranslateService, TranslatePipe } from '@ngx-translate/core';
import { ReadPopoverPage } from '../read-popover/read-popover';
import { LanguageService } from '../../app/language/language.service';

@IonicPage({
  name: 'single-edition',
  segment: 'edition/:id'
})
@Component({
  selector: 'page-single-edition',
  templateUrl: 'single-edition.html'
})
export class SingleEditionPage {

  edition: DigitalEdition;
  shallFetch: boolean;
  errorMessage: string;
  image: string;

  appName: string;
  subTitle: string;
  tableOfContents: TableOfContentsCategory[];
  tocItems: GeneralTocItem[];
  parentItem: GeneralTocItem;
  root: TableOfContentsCategory[];
  items: any;
  editionDescription: any;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public params: NavParams,
    private tableOfContentsService: TableOfContentsService,
    public popoverCtrl: PopoverController,
    private config: ConfigService,
    private textService: TextService,
    private htmlService: HtmlContentService,
    private translate: TranslateService,
    private app: App,
    private langService: LanguageService
  ) {

        this.edition = this.params.get('edition') || {id: this.params.get('id')};
        this.parentItem = this.params.get('tocItem');
        this.shallFetch = this.params.get('fetch');
        this.langService.getLanguage().subscribe((lang) => {
          this.appName = this.config.getSettings('app.name.' + lang);
        });
        this.editionDescription = {content: null};

    if ( this.parentItem !== undefined ) {
      this.subTitle = this.parentItem.title;
    } else {
      this.subTitle = this.config.getSettings('single-editions')[this.config.getSettings('i18n').locale].toc;
    }

    if ( this.edition !== undefined && this.edition.id !== undefined ) {
      if (this.edition.title !== undefined) {
        global.setSubtitle(this.edition.title);
      }
      this.getEditionDescription(this.edition.id);
    }

    this.edition.title = global.getSubtitle();

        if (!this.shallFetch && this.params.data.tocItem && this.params.data.tocItem.items ) {
            this.items = this.params.data.tocItem.items;
            this.root = this.params.data.root;
        }
        const editionImages = this.config.getSettings('editionImages');
        this.image = editionImages[this.edition.id];

  }

  ionViewWillEnter() {
    if (this.edition.id) {
      this.getTocRoot(this.edition.id);
      // this.getIntroduction(this.edition.id);
    } else {
      this.getTocGroup(this.parentItem.toc_ed_id, this.parentItem.toc_id);
    }
    this.viewCtrl.setBackButtonText('');
  }

  // getIntroduction(id: string, lang:string) {
  //  this.textService.getIntroduction(id, lang);
  // }

  setTocItemTitleLevel(tocItems) {
    let maxLevel = tocItems[0].titleLevel;
    let minLevel = tocItems[0].titleLevel;
    let allSameLevel = true;
    for ( const item in tocItems ) {
      if ( tocItems[item].titleLevel > maxLevel ) {
        maxLevel = tocItems[item].titleLevel;
        allSameLevel = false;
      }

      if ( tocItems[item].titleLevel < minLevel ) {
        minLevel = tocItems[item].titleLevel;
      }
    }

    for ( const item in tocItems ) {
      tocItems[item].maxTitleLevel = maxLevel;
      tocItems[item].minTitleLevel = minLevel;
      tocItems[item].isIndented = false;
      tocItems[item].isHighLevel = false;
      if ( allSameLevel === false && tocItems[item].titleLevel === maxLevel ) {
        tocItems[item].isIndented = true;
      }

      if ( tocItems[item].titleLevel === minLevel && (maxLevel - minLevel) >= 2 ) {
        tocItems[item].isHighLevel = true;
      }
    }
    return tocItems;
  }

  getTocRoot(id: string) {
    this.tableOfContentsService.getTableOfContentsRoot(id)
        .subscribe(
            tocItems => {
              this.tocItems = this.setTocItemTitleLevel(tocItems);
            },
            error =>  {this.errorMessage = <any>error});
  }



  getTocGroup(id: string, group_id: string) {
    this.tableOfContentsService.getTableOfContentsGroup(id, group_id)
        .subscribe(
            tocItems => {
              this.tocItems = this.setTocItemTitleLevel(tocItems);
            },
            error =>  {this.errorMessage = <any>error});
  }

  getTableOfContents(id: string) {
    this.tableOfContentsService.getTableOfContents(id)
        .subscribe(
            tableOfContents => {
              this.root = tableOfContents;
              this.tableOfContents = tableOfContents;
            },
            error =>  {this.errorMessage = <any>error});
  }

  getEditionDescription(id: string) {
    this.htmlService.getHtmlContent(id + '_desc')
        .subscribe(
            editionDescription => {
              this.editionDescription = editionDescription;
            },
            error =>  {this.errorMessage = <any>error});
  }

  showPopover(myEvent) {
    const popover = this.popoverCtrl.create(ReadPopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  openFirstPage() {

    const params = {tocItem: null, fetch: false, edition: {title: this.subTitle}};
    params['editionId'] = this.params.get('id')
    params['id'] = 'first';

    const nav = this.app.getActiveNavs();
    nav[0].push('read', params, {animate: true, direction: 'forward', animation: 'ios-transition'});

  }
}
