import { Component, Renderer, ElementRef, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { App, ViewController, NavController, NavParams, PopoverController, ActionSheetController,
         ToastController, ModalController, IonicPage } from 'ionic-angular';
import { TranslateModule, LangChangeEvent, TranslateService, TranslatePipe } from '@ngx-translate/core';

import { EstablishedText } from '../../app/texts/established-text.model';
import { ConfigService } from '@ngx-config/core';
import { TextService } from '../../app/texts/text.service';
import { CommentService } from '../../app/comments/comment.service';

import { Tooltip } from '../../app/tooltip/tooltip.model';
import { TooltipService } from '../../app/tooltip/tooltip.service';

import { TableOfContentsCategory, GeneralTocItem } from '../../app/table-of-contents/table-of-contents.model';
import { TableOfContentsService } from '../../app/table-of-contents/table-of-contents.service';
import { TableOfContentsItem } from '../../app/table-of-contents/table-of-contents-item.model';
import { ReadPopoverPage } from '../read-popover/read-popover';
import { ReadPopoverService, Fontsize } from '../read-popover/read-popover.service';

import { CommentModalPage } from '../comment-modal/comment-modal';
import { SemanticDataModalPage } from '../semantic-data-modal/semantic-data-modal';

import { global } from '../../app/global';

import { SingleEditionPage } from '../../pages/single-edition/single-edition';
import { LanguageService } from '../../app/language/language.service';

@IonicPage({
  name: 'read',
  segment: 'text/:editionId/:id'
})
@Component({
  selector: 'page-read',
  templateUrl: './read.html'
})
export class ReadPage /*implements OnDestroy*/ {

  listenFunc: Function;
  show = 'establishedText';

  establishedText: EstablishedText;
  errorMessage: string;
  appName: string;
  tocRoot: TableOfContentsCategory[];
  popover: ReadPopoverPage;

  tooltipContent: any;
  subTitle: string;

  prevnext: any;
  texts: any;

  constructor(  private app: App,
                public viewCtrl: ViewController,
                public navCtrl: NavController,
                public params: NavParams,
                private textService: TextService,
                private commentService: CommentService,
                public toastCtrl: ToastController,
                private renderer: Renderer,
                private elementRef: ElementRef,
                private config: ConfigService,
                public popoverCtrl: PopoverController,
                public readPopoverService: ReadPopoverService,
                public actionSheetCtrl: ActionSheetController,
                public modalCtrl: ModalController,
                private sanitizer: DomSanitizer,
                private tooltipService: TooltipService,
                private tocService: TableOfContentsService,
                public translate: TranslateService,
                private langService: LanguageService) {

    let id = null;
    let link = null;

    if ( this.params.get('tocItem') !== undefined ) {
      // @TODO: fix this. it is unmaintainable
      id = this.params.get('tocItem').toc_id;
      const editionIsUndefined = (this.params.get('tocItem').edition_id !== undefined);
      const linkIdIsNotUndefined = ( this.params.get('tocItem').link_id !== undefined );
      const editionId = this.params.get('tocItem').edition_id;

      link = (editionIsUndefined ? editionId : this.params.get('tocItem').toc_ed_id) + '_'
      + ( this.params.get('tocItem').link_id || this.params.get('tocItem').toc_linkID);

    } else if ( this.params.get('editionId') !== undefined && this.params.get('id') !== undefined ) {
      id = this.params.get('id');
      link = this.params.get('editionId') + '_' + id;
    }

    const title = global.getSubtitle();
    this.tocRoot = this.params.get('root');


    this.establishedText = new EstablishedText({link: link, id: id, title: title, text: ''});

    this.setUpTextListeners();

    this.updateTexts()
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.updateTexts();
    });

  }

  private setUpTextListeners() {
    // We must do it like this since we want to trigger an event on a dynamically loaded innerhtml.

    this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {

      if (event.target.classList.contains('tooltiptrigger')) {
        if (event.target.hasAttribute('data-id')) {
          if (event.target.classList.contains('person') && this.readPopoverService.show.personInfo) {
            this.showPersonModal(event.target.getAttribute('data-id'));
          } else if (event.target.classList.contains('placeName') && this.readPopoverService.show.placeInfo) {
            this.showPlaceModal(event.target.getAttribute('data-id'));
          } else if (event.target.classList.contains('comment') && this.readPopoverService.show.comments ) {
            this.showCommentModal(event.target.getAttribute('data-id'));
          }
        } else {
          this.showTooltip(event.target.nextSibling.innerHTML);
        }
      }
    });
  }

  private showIntroduction() {
    this.establishedText.content = '';
    this.getIntroduction(this.params.get('editionId'), this.translate.currentLang);
  }

  private showText() {
    this.getEstablishedText(this.establishedText.link);
    this.getPrevNext(this.establishedText.link.replace('_est', ''));
  }

  private showFirstText() {
    this.tocService.getFirst(this.params.get('editionId')).subscribe(
      first => {
        this.establishedText.content = '';
        this.establishedText.title = first[0].title;
        this.establishedText.link = first[0].toc_ed_id + '_' + first[0].toc_linkID;
        this.showText();
      },
      error => {
        console.log('error');
      }
    );
  }

  ionViewWillEnter() {
    if (this.params.get('id') === 'first') {
      this.showFirstText();
    } else if (this.params.get('id') === 'introduction') {
      this.showIntroduction();
    } else {
      this.showText();
    }
    this.viewCtrl.setBackButtonText('');
  }

  updateTexts() {
    this.translate.get('Read').subscribe(
      value => {
        this.texts = value;
      }
    )
    this.langService.getLanguage().subscribe((lang) => {
      this.appName = this.config.getSettings('app.name.' + lang);
    });
  }


  setText(id, data) {

    const id2 = id.replace('_est', '');
    const parts = id2.split(';');

    let tmpContent = data;

      if (parts.length > 1) {

        let selector = '#' + parts[1];
        if (parts.length > 2) {
          selector = '.' + parts[2];
        }

        const range = document.createRange();
        const documentFragment = range.createContextualFragment(data);

        tmpContent = documentFragment.querySelector(selector).innerHTML || '';
      }

      this.establishedText.content =

      tmpContent
      .replace(/images\//g, 'assets/images/tei/')
      .replace(/\.png/g, '.svg');
  }


  getEstablishedText(id: string) {
      this.textService.getEstablishedText(id).subscribe(
          text => {
              // in order to get id attributes for tooltips
              this.establishedText.content = this.sanitizer.bypassSecurityTrustHtml (
                text.replace(/images\//g, 'assets/images/tei/')
                    .replace(/\.png/g, '.svg')
              );
            },
          error =>  {this.errorMessage = <any>error}
        );
  }

  getIntroduction(id: string, lang: string) {
    this.textService.getIntroduction(id, lang).subscribe(
      res => {
          // in order to get id attributes for tooltips
          this.establishedText.content = this.sanitizer.bypassSecurityTrustHtml (
            res.content.replace(/images\//g, 'assets/images/tei/')
                .replace(/\.png/g, '.svg')
          );
        },
      error =>  {this.errorMessage = <any>error}
    );
  }

  getPrevNext(id: string) {
      this.tocService.getPrevNext(id).subscribe(
          prevnext => {
              // in order to get id attributes for tooltips
              this.prevnext = prevnext;
            },
          error =>  {this.errorMessage = <any>error}
        );
  }


  showPersonTooltip(id: string) {
      this.tooltipService.getPersonTooltip(id).subscribe(
          tooltip => {
             this.showTooltip(tooltip.description);
          },
          error =>  {
              this.showTooltip('Could not get person information');
          }
      );
  }

  showPlaceTooltip(id: string) {
    this.tooltipService.getPlaceTooltip(id).subscribe(
        tooltip => {
            this.showTooltip(tooltip.description);
        },
        error =>  {
            this.showTooltip('Could not get place information');
        }
    );
  }

  showCommentModal(id: string) {
    id = id.replace('end', 'en');
    id = this.establishedText.link + ';' + id;
    const modal = this.modalCtrl.create(CommentModalPage, {id: id, title: this.texts.CommentsFor + ' ' + this.establishedText.title});
    modal.present();

  }

  showPersonModal(id: string) {
    const modal = this.modalCtrl.create(SemanticDataModalPage, {id: id, type: 'person'});
    modal.present();
  }

  showPlaceModal(id: string) {
    const modal = this.modalCtrl.create(SemanticDataModalPage, {id: id, type: 'place'});
    modal.present();
  }





  showCommentTooltip(id: string) {

    id = this.establishedText.link + ';' + id;
    this.tooltipService.getCommentTooltip(id).subscribe(
        tooltip => {
            this.showTooltip(tooltip.description);
        },
        error =>  {
            this.showTooltip('Could not get comment');
        }
    );
  }

  showTooltip(text: string) {
    /*let toast = this.toastCtrl.create({
      message: text,
      duration: 6000,
      showCloseButton: true,
      dismissOnPageChange: true,
      closeButtonText: "Stäng"
    });
    toast.present();*/
  }

  showPopover(myEvent) {
    const popover = this.popoverCtrl.create(ReadPopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  presentDownloadActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Ladda ner digital version',
      buttons: [
        {
          text: 'Epub',
          role: 'epub',
          handler: () => {
            console.log('Epub clicked');
          }
        }, {
          text: 'Kindle',
          role: 'kindle',
          handler: () => {
            console.log('Kindle clicked');
          }
        }, {
          text: 'PDF',
          role: 'pdf',
          handler: () => {
            console.log('PDF clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  firstPage() {
    this.tocService.getFirst(this.params.get('editionId')).subscribe(
      first => {
          this.openAnother(first[0], 'forward');
        },
      error =>  {this.errorMessage = <any>error}
    );
  }

  nextPage() {
    if ( this.prevnext !== undefined ) {
      this.openAnother(this.prevnext.next, 'forward');
    }
  }

  prevPage() {
    if ( this.prevnext !== undefined ) {
      this.openAnother(this.prevnext.prev, 'back');
    }
  }

  swipePrevNext(myEvent) {
    if ( myEvent['offsetDirection'] !== undefined ) {
      if ( myEvent['offsetDirection'] === 2 ) {
        this.nextPage();
      } else if ( myEvent['offsetDirection'] === 4 ) {
        this.prevPage();
      }
    }
  }

  openAnother(tocItem: any, direction= 'forward') {
    const params = {root: this.tocRoot, tocItem: tocItem, fetch: false, edition: {title: tocItem.title}};
    params['editionId'] = tocItem.edition_id;
    params['id'] = tocItem.link_id;

    const nav = this.app.getActiveNavs();
    nav[0].push('read', params, {animate: true, direction: direction, animation: 'ios-transition'}).then(() => {
      // This is so that we can always hace only one text in the stack, so that
      // when we press the back button i nav menu, we go to the table of contents
      // instead of the previous text we read.
      // this allows us to go to previous/next texts with the custom arrows
      const index = nav[0].getActive().index;
      nav[0].remove(index - 1); // we remove the last text we read from the stack.
                              // so that "back" is always the table of contents.
    });

  }

  findItem(id: string, includePrevNext?: boolean): any {
      let prev: any;
      const returnData = {item: TableOfContentsItem, next: TableOfContentsItem, prev: TableOfContentsItem};

      let found = false;
      let cat, child, item;

      for (cat of this.tocRoot) {
        for (child of cat.items) {
            for (item of child.items) {
                if (found) { // we found it last iteration...
                    returnData.next = item;
                    return returnData;
                }

                if (item.id === id) {
                    found = true;
                    returnData.item = item;
                    returnData.prev = prev;
                    if (!includePrevNext) {
                        return item;
                    }
                }
                prev = item;
            }
        }
      }

      if (found) {
          return includePrevNext ? returnData : returnData.item;
      }
    }
}
