import { Component } from '@angular/core';
import { TranslateModule, LangChangeEvent, TranslateService, TranslatePipe } from '@ngx-translate/core';
import { IonicPage, NavParams } from 'ionic-angular';

import { } from '../home/home.module';
import { } from '../about/about.module';
import { } from '../editions/editions.module';

@IonicPage({
  segment: 'digital-edition'
})
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root = 'home';
  tab2Root = 'EditionsPage';
  tab3Root = 'about';

  language = 'sv';
  texts: any = {Home: '1', Read: '2', Info : '3'};

  constructor(navParams: NavParams, public translate: TranslateService) {
    this.updateTexts()
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.updateTexts();
    });
  }

  updateTexts() {
    this.translate.get('Tabs').subscribe(
      value => {
        this.texts = value;
      }
    )
  }
}
