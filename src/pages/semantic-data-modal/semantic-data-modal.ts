import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { SemanticDataService } from '../../app/semantic-data/semantic-data.service';

/*
  Generated class for the SemanticDataModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-semantic-data-modal',
  templateUrl: 'semantic-data-modal.html'
})
export class SemanticDataModalPage {

  public semanticData: any;
  public title: string;

  constructor(  public navCtrl: NavController,
                public viewCtrl: ViewController,
                params: NavParams,
                private sanitizer: DomSanitizer,
                private semanticDataService: SemanticDataService

  ) {
    const id = params.get('id');
    const id_parts = id.split(';');
    this.title = params.get('title');
    const type = params.get('type');

    if (type === 'place') {
      this.getPlace(id);
    }

  if (type === 'person') {
      this.getPerson(id);
    }
  }



  ionViewDidLoad() {

  }


  getPlace(id: string) {
      this.semanticDataService.getPlace(id).subscribe(
          data => {
            // in order to get id attributes for tooltips
              this.semanticData = this.sanitizer.bypassSecurityTrustHtml (
                data.replace(/images\//g, 'assets/images/tei/')
                    .replace(/\.png/g, '.svg')
              );

            },
          error =>  {
              this.semanticData = 'Unable to get semanticData';
          }
        );
  }

  getPerson(id: string) {
      this.semanticDataService.getPerson(id).subscribe(
          data => {
        this.title = data['title'];
              // in order to get id attributes for tooltips
              this.semanticData = this.sanitizer.bypassSecurityTrustHtml (
                // @TODO: Refactor this.
                data['content'].replace(/images\//g, 'assets/images/tei/')
                    .replace(/\.png/g, '.svg')
                    .replace(((data['c_webbefternamn1'] != null) ? data['c_webbefternamn1'] : '')
                    + ((data['c_webbefternamn1'] != null && data['c_webbfornamn1'] != null) ? ', ' : '')
                    + ((data['c_webbfornamn1'] != null) ? data['c_webbfornamn1'] : ''), '<b>'
                    + ((data['c_webbefternamn1'] != null) ? data['c_webbefternamn1'] : '')
                    + ((data['c_webbefternamn1'] != null && data['c_webbfornamn1'] != null) ? ', ' : '')
                    + ((data['c_webbfornamn1'] != null) ? data['c_webbfornamn1'] : '') + '</b>')
              );

            },
          error =>  {
              this.semanticData = 'Unable to get semanticData';
          }
        );
  }

  getSemanticData(id: string) {
      this.semanticData = 'Loading semanticData ..';
      this.semanticDataService.getSemanticData(id).subscribe(
          data => {
              this.semanticData = this.sanitizer.bypassSecurityTrustHtml (
                data.replace(/images\//g, 'assets/images/tei/')
                    .replace(/\.png/g, '.svg')
              );
            },
          error =>  {
              this.semanticData = 'Unable to get semanticData';
          }
        );
  }

   dismiss() {
     this.viewCtrl.dismiss();
   }
}
