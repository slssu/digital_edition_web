import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditionsPage } from './editions';
import { DigitalEditionList } from '../../app/digital-edition-list/digital-edition-list.component';
import { DigitalEditionListService } from '../../app/digital-edition-list/digital-edition-list.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http} from '@angular/http';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
      EditionsPage,
      DigitalEditionList
    ],
    imports: [
      IonicPageModule.forChild(EditionsPage),
      TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [Http]
        }
      })
    ],
    entryComponents: [
      EditionsPage
    ],
    providers: [
      DigitalEditionListService
    ]
  })
  export class EditionsPageModule {}
