import { Component } from '@angular/core';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { ConfigService } from '@ngx-config/core';
import { HtmlContentService } from '../../app/html-content/html-content.service';
import { LanguageService } from '../../app/language/language.service';

@IonicPage({
  segment: 'publications'
})
@Component({
  selector: 'editions-page',
  templateUrl: 'editions.html'
})
export class EditionsPage {
  selectedItem: any;
  appName: string;
  readContent: string;
  errorMessage: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private config: ConfigService,
    private htmlContentService: HtmlContentService,
    public translate: TranslateService,
    public languageService: LanguageService
  ) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(EditionsPage, {
      item: item
    });
  }

  ionViewWillEnter() {
    this.languageService.getLanguage().subscribe((lang: string) => {
      this.getHtmlContent('editions-' + lang);
      this.appName = this.config.getSettings('app.name.' + lang);
    });
  }

  getHtmlContent(filename: string) {
    this.htmlContentService.getHtmlContent(filename)
        .subscribe(
            text => {this.readContent = text.content; },
            error =>  {this.errorMessage = <any>error});
  }
}
