import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';

export const enum Fontsize {
    small,
    large
}

@Injectable()
export class ReadPopoverService {

  show = {
    'comments': true,
    'personInfo': false,
    'abbreviations': false,
    'placeInfo': false,
    'pageNumbering': false
  };

  fontsize = Fontsize.small;


  constructor(private config: ConfigService) {

  }


}
