import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReadPopoverPage } from './read-popover.ts';
import { ReadPopoverService } from './read-popover.service';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Http } from '@angular/http';

export function createTranslateLoader(http: Http): TranslateLoader {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        ReadPopoverPage
    ],
    imports: [
        IonicPageModule.forChild(ReadPopoverPage),

        TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (createTranslateLoader),
              deps: [Http]
            }
          }),
    ],
    entryComponents: [
        ReadPopoverPage
    ],
    providers: [
        ReadPopoverService
     ]
  })
  export class ReadPopoverPageModule {}
