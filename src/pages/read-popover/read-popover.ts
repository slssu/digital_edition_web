import { Component } from '@angular/core';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';

import { ViewController } from 'ionic-angular';
import { ConfigService } from '@ngx-config/core';

import { ReadPopoverService, Fontsize } from './read-popover.service';

/*@IonicPage({
  name: 'read-popover-page'
})*/
@Component({
  selector: 'read-popover-page',
  templateUrl: 'read-popover.html'
})
export class ReadPopoverPage {
  readToggles: {
      'comments': boolean,
      'personInfo': boolean,
      'placeInfo': boolean,
      'abbreviations': boolean,
      'pageNumbering': boolean
  };

  show = {
      'comments': false,
      'personInfo': false,
      'placeInfo': false,
      'abbreviations': false,
      'pageNumbering': false
  };

  fontsize = null;

  constructor(
    public viewCtrl: ViewController,
    private config: ConfigService,
    public readPopoverService: ReadPopoverService,
    public translate: TranslateService
  ) {
    this.readToggles = this.config.getSettings('settings.readToggles');
    this.show = readPopoverService.show;
    this.fontsize = readPopoverService.fontsize;

  }

  close() {
    this.viewCtrl.dismiss();
  }

  toggleComments() {
    this.readPopoverService.show.comments = this.show.comments;
  }

  togglePersonInfo() {
    this.readPopoverService.show.personInfo = this.show.personInfo;
  }

  toggleAbbreviations() {
    this.readPopoverService.show.abbreviations = this.show.abbreviations;
  }

  togglePageNumbering() {
    this.readPopoverService.show.pageNumbering = this.show.pageNumbering;
  }

  decreaseFontSize() {
    this.fontsize = Fontsize.small;
    this.readPopoverService.fontsize = this.fontsize;
  }

  increaseFontSize() {
    this.fontsize = Fontsize.large;
    this.readPopoverService.fontsize = this.fontsize;
  }
}
