import { Component, Renderer } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ConfigService } from '@ngx-config/core';
import { HtmlContent } from '../../app/html-content/html-content.model';

import { HtmlContentService } from '../../app/html-content/html-content.service';
import { IonicPage } from 'ionic-angular';

import { LanguageService } from '../../app/language/language.service';

@IonicPage({
  name: 'content',
  segment: 'about/:index'
})
@Component({
  selector: 'page-content',
  templateUrl: 'content.html'
})
export class ContentPage /*implements OnDestroy*/ {

  htmlContent: HtmlContent;
  errorMessage: string;
  appName: string;

  constructor(  public navCtrl: NavController,
                public params: NavParams,
                private htmlContentService: HtmlContentService,
                renderer: Renderer,
                private config: ConfigService,
                private langService: LanguageService) {

    const data = this.config.getSettings('staticPages.about');
    const index = this.params.get('index');
    this.htmlContent = new HtmlContent({title: '...', text: null, filename: null});

    this.langService.getLanguage().subscribe((lang) => {
      this.appName = this.config.getSettings('app.name.' + lang);
      const filename = data[index][lang].file;
      const title = data[index][lang].name;
      this.htmlContent = new HtmlContent({filename: filename, title: title, text: ''});
    });
  }

  ionViewWillEnter() {
    this.getHtmlContent(this.htmlContent.filename);
  }

  getHtmlContent(filename: string) {
    this.htmlContentService.getHtmlContent(filename)
        .subscribe(
            text => {this.htmlContent.content = text.content; },
            error =>  {this.errorMessage = <any>error});
  }
}
