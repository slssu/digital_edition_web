import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentPage } from './content';
import { HtmlContent } from '../../app/html-content/html-content.model';
import { HtmlContentService } from '../../app/html-content/html-content.service';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http} from '@angular/http';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
      ContentPage
    ],
    imports: [
      IonicPageModule.forChild(ContentPage),
      TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [Http]
        }
      })
    ],
    entryComponents: [
      ContentPage
    ],
    providers: [
        HtmlContentService
    ]
  })
  export class ContentPageModule {}
