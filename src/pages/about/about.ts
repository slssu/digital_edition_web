import { Component } from '@angular/core';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { App, NavController, IonicPage } from 'ionic-angular';
import { ConfigService } from '@ngx-config/core';
import { LanguageService } from '../../app/language/language.service';

import {  } from '../../pages/content/content';

@IonicPage({
  name: 'about',
  segment: 'about'
})
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  aboutPages: any;
  language = 'sv';
  languages = [];
  appName: string;
  enableLanguageChanges: false;

  constructor(
    private app: App,
    public navCtrl:
    NavController,
    private config: ConfigService,
    public translate: TranslateService,
    public languageService: LanguageService
  ) {
    this.aboutPages = this.config.getSettings('staticPages.about');
    this.enableLanguageChanges = this.config.getSettings('i18n.enableLanguageChanges');
    this.languages = this.config.getSettings('i18n.languages');

    this.languageService.getLanguage().subscribe((lang: string) => {
      this.language = lang;
      this.appName = this.config.getSettings('app.name.' + lang);
    });
  }

  ionViewDidLoad() {

  }

  openContentPage(index: any) {
    const nav = this.app.getActiveNavs();
    const params = {index: index, lang: this.language};
    nav[0].push('content', params, {animate: true, direction: 'forward', animation: 'ios-transition'});
  }

  changeLanguage() {
    this.translate.use(this.language);
  }

}
