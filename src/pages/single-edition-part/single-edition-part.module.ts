import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleEditionPagePart } from './single-edition-part';
import { TableOfContentsModule } from '../../app/table-of-contents/table-of-contents.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

@NgModule({
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    declarations: [
        SingleEditionPagePart
    ],
    imports: [
      IonicPageModule.forChild(SingleEditionPagePart),
      TableOfContentsModule
    ],
    entryComponents: [
        SingleEditionPagePart
    ]
  })
  export class SingleEditionPagePartModule {}
