import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, PopoverController, IonicPage } from 'ionic-angular';

import { DigitalEdition } from '../../app/digital-edition/digital-edition.model';
import { TableOfContentsCategory, GeneralTocItem } from '../../app/table-of-contents/table-of-contents.model';
import { TableOfContentsService } from '../../app/table-of-contents/table-of-contents.service';
import { TextService } from '../../app/texts/text.service';
import { ReadPopoverPage } from '../read-popover/read-popover';
import { ConfigService } from '@ngx-config/core';
import { global } from '../../app/global';
import { Events } from 'ionic-angular';
import { TranslateService, TranslatePipe } from '@ngx-translate/core';
import { LanguageService } from '../../app/language/language.service';

@IonicPage({
  name: 'single-edition-part',
  segment: 'edition/:editionId/:id'
})
@Component({
  selector: 'page-single-edition-part',
  templateUrl: 'single-edition-part.html'
})
export class SingleEditionPagePart {

  edition: DigitalEdition;
  shallFetch: boolean;
  errorMessage: string;
  image: string;

  appName: string;
  subTitle: string;

  tableOfContents: TableOfContentsCategory[];
  tocItems: GeneralTocItem[];
  parentItem: GeneralTocItem;
  root: TableOfContentsCategory[];
  items: any;

  constructor(public events: Events,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public params: NavParams,
    private tableOfContentsService: TableOfContentsService,
    public popoverCtrl: PopoverController,
    private config: ConfigService,
    private textService: TextService,
    private translate: TranslateService,
    private langService: LanguageService) {

      console.log('at single edition part...');

      this.edition = this.params.get('edition') || {id: this.params.get('editionId')};
      this.parentItem = this.params.get('tocItem');
      this.shallFetch = this.params.get('fetch');

      this.langService.getLanguage().subscribe((lang) => {
        this.appName = this.config.getSettings('app.name.' + lang);
      });

    if ( this.parentItem !== undefined ) {
      this.subTitle = this.parentItem.title;
    }

    if ( this.edition !== undefined && this.edition.title !== undefined ) {
      global.setSubtitle(this.edition.title);
    }

    this.edition.title = global.getSubtitle();

        if (!this.shallFetch && this.params.data.tocItem && this.params.data.tocItem.items ) {
            this.items = this.params.data.tocItem.items;
            this.root = this.params.data.root;
        }
        const editionImages = this.config.getSettings('editionImages');
        this.image = editionImages[this.edition.id];

  }
  ionViewWillEnter() {
    if (this.edition.id) {
      this.getTocRoot(this.edition.id);
    } else {
      this.getTocGroup(this.parentItem.toc_ed_id, this.parentItem.toc_id);
    }
    this.viewCtrl.setBackButtonText('');
  }

  setTocItemTitleLevel(tocItems) {
    let maxLevel = tocItems[0].titleLevel;
    let minLevel = tocItems[0].titleLevel;
    let allSameLevel = true;
    for ( const item in tocItems ) {
      if ( tocItems[item].titleLevel > maxLevel ) {
        maxLevel = tocItems[item].titleLevel;
        allSameLevel = false;
      }

      if ( tocItems[item].titleLevel < minLevel ) {
        minLevel = tocItems[item].titleLevel;
      }
    }

    for ( const item in tocItems ) {
      tocItems[item].maxTitleLevel = maxLevel;
      tocItems[item].minTitleLevel = minLevel;
      tocItems[item].isIndented = false;
      tocItems[item].isHighLevel = false;
      if ( allSameLevel === false && tocItems[item].titleLevel === maxLevel ) {
        tocItems[item].isIndented = true;
      }

      if ( tocItems[item].titleLevel === minLevel && (maxLevel - minLevel) >= 2 ) {
        tocItems[item].isHighLevel = true;
      }
    }
    return tocItems;
  }

  getTocRoot(id: string) {
    this.tableOfContentsService.getTableOfContentsRoot(id)
        .subscribe(
            tocItems => {
              this.tocItems = this.setTocItemTitleLevel(tocItems);
            },
            error =>  {this.errorMessage = <any>error});
  }

  getTocGroup(id: string, group_id: string) {
    this.tableOfContentsService.getTableOfContentsGroup(id, group_id)
        .subscribe(
            tocItems => {
              this.tocItems = this.setTocItemTitleLevel(tocItems);
            },
            error =>  {this.errorMessage = <any>error});
  }

  getTableOfContents(id: string) {
    this.tableOfContentsService.getTableOfContents(id)
        .subscribe(
            tableOfContents => {
              this.root = tableOfContents;
              this.tableOfContents = tableOfContents;
            },
            error =>  {this.errorMessage = <any>error});
  }

  showPopover(myEvent) {
    const popover = this.popoverCtrl.create(ReadPopoverPage);
    popover.present({
      ev: myEvent
    });
  }
}
