import { Component } from '@angular/core';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { NavController, IonicPage } from 'ionic-angular';
import { ConfigService } from '@ngx-config/core';
import { HtmlContentService } from '../../app/html-content/html-content.service';
import { LanguageService } from '../../app/language/language.service';
import { Title } from '@angular/platform-browser';

@IonicPage({
  name: 'home',
  segment: 'home'
})
@Component({
  selector: 'home-page',
  templateUrl: 'home.html'
})
export class HomePage {
  appName: string;
  appSubtitle: string;
  appMachineName: string;
  homeContent: string;
  errorMessage: string;
  initLanguage: string;

  constructor(
    public navCtrl: NavController,
    private config: ConfigService,
    private htmlContentService: HtmlContentService,
    public translate: TranslateService,
    public languageService: LanguageService,
    private titleService: Title
  ) {
    this.appMachineName = this.config.getSettings('app.machineName');
    this.titleService.setTitle(' | ' + this.appName);
  }

  ionViewWillEnter() {
    this.languageService.getLanguage().subscribe((lang: string) => {
      this.getHtmlContent('frontpage-' + lang);
      this.appName = this.config.getSettings('app.name.' + lang);
      this.appSubtitle = this.config.getSettings('app.subTitle.' + lang);
    });
  }

  getHtmlContent(filename: string) {
    this.htmlContentService.getHtmlContent(filename)
        .subscribe(
            text => {this.homeContent = text.content; },
            error =>  {this.errorMessage = <any>error});
  }
}
